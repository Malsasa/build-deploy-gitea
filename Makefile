
HOSTNAME_FQDN := codeberg-test.org

export PATCHDIR := ${PWD}/local_patches
export BUILDDIR := ${PWD}/build
export GOROOT := ${BUILDDIR}/go
export GOPATH := ${BUILDDIR}/gitea
export PATH := ${GOROOT}/bin:${GOPATH}/bin:${PATH}

GOTAR = go1.13.8.$(shell uname | tr [:upper:] [:lower:])-amd64.tar.gz
ORIGIN = https://codeberg.org/Codeberg/gitea
GITEA_BRANCH := codeberg

IMAGE_PREFIX = etc/gitea/public/img
TARGETS = \
	${IMAGE_PREFIX}/logo.svg \
	${IMAGE_PREFIX}/logo-small.svg \
	${IMAGE_PREFIX}/logo-medium.svg \
	${IMAGE_PREFIX}/favicon.ico \
	${IMAGE_PREFIX}/favicon.png \
	${IMAGE_PREFIX}/codeberg.png \
	${IMAGE_PREFIX}/gitea-sm.png \
	${IMAGE_PREFIX}/gitea-lg.png \
	${IMAGE_PREFIX}/gitea-safari.svg \
	${GOPATH}/bin/gitea

all : ${TARGETS}

${GOPATH}/bin/gitea : ${GOPATH}/src/code.gitea.io/gitea
	TAGS=bindata make -j1 -C $< generate build install

${GOPATH}/src/code.gitea.io/gitea : ${GOROOT}/bin/go
	${GOROOT}/bin/go get -d -u -v code.gitea.io/gitea
	( cd $@ && git remote rename origin github-gitea )
	( cd $@ && git remote add origin ${ORIGIN} )
	( cd $@ && git fetch origin)
	( cd $@ && git checkout origin/${GITEA_BRANCH} )
	( cd $@ && cat $(wildcard ${PATCHDIR}/*.diff) | patch -p1 )

${GOROOT}/bin/go :
	mkdir -p ${GOROOT}/Downloads
	wget -c --no-verbose --directory-prefix=${GOROOT}/Downloads https://dl.google.com/go/${GOTAR}
	tar xfz ${GOROOT}/Downloads/${GOTAR} -C ${BUILDDIR}

deployment : deploy-gitea

deploy-gitea : ${GOPATH}/bin/gitea ${TARGETS}
	ssh root@${HOSTNAME_FQDN} mkdir -p /data/git/bin
	scp $< root@${HOSTNAME_FQDN}:/data/git/bin/gitea.new
	ssh root@${HOSTNAME_FQDN} mv /data/git/bin/gitea.new /data/git/bin/gitea
	-ssh root@${HOSTNAME_FQDN} systemctl stop gitea
	./deployConfigFiles.sh ${HOSTNAME_FQDN}
	scp -r var/www/* root@${HOSTNAME_FQDN}:/var/www/
	ssh root@${HOSTNAME_FQDN} systemctl daemon-reload
	ssh root@${HOSTNAME_FQDN} systemctl enable gitea
	ssh root@${HOSTNAME_FQDN} systemctl start gitea
	ssh root@${HOSTNAME_FQDN} systemctl status gitea

${IMAGE_PREFIX}/logo.svg : codeberg.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/logo-medium.svg : codeberg.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	inkscape $@ --select=layer3 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/logo-small.svg : codeberg.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	inkscape $@ --select=layer2 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --select=layer3 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/favicon.png : ${IMAGE_PREFIX}/logo-small.svg
	convert -background none -density 256 -resize 256x256 -gravity center -extent 256x256 $< $@

${IMAGE_PREFIX}/favicon.ico : ${IMAGE_PREFIX}/favicon.png
	convert -background none -density 256 $< -define icon:auto-resize $@

${IMAGE_PREFIX}/codeberg.png : ${IMAGE_PREFIX}/logo-small.svg
	convert -background none -density 290 -resize 290x290 -gravity center -extent 290x290 $< $@

${IMAGE_PREFIX}/gitea-sm.png : ${IMAGE_PREFIX}/logo-small.svg
	convert -background none -density 120 -resize 120x120 -gravity center -extent 120x120 $< $@

${IMAGE_PREFIX}/gitea-lg.png : ${IMAGE_PREFIX}/logo.svg
	convert -background none -density 1200 -resize 1200x1200 -gravity center -extent 1200x1200 $< $@

${IMAGE_PREFIX}/gitea-safari.svg : ${IMAGE_PREFIX}/logo-medium.svg
	cp -v $< $@

clean :
	${MAKE} -C ${GOPATH}/src/code.gitea.io/gitea clean
	${RM} ${TARGETS}

realclean :
	${RM} ${TARGETS}
	rm -rf ${BUILDDIR}

